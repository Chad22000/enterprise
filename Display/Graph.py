import numpy as np
import matplotlib.pyplot as plt


class BaseGraph:
    def __init__(self):
        self.title = "Your graph title"
        self.x_label = "X-axis label"
        self.y_label = "X-axis label"
        self.show_grid = True

    def show(self, x_values, y_values):
        #RECUPERATION DES VALEURS X ET Y DEPUIS L'OBJET ZONES
        plt.plot(y_values, x_values)
        plt.xlabel(self.x_label)
        plt.ylabel(self.y_label)
        plt.title(self.title)
        plt.grid(self.show_grid)
        # AFFICHAGE DU TABLEAU
        plt.show()

    def xy_values(self, zones):
        raise NotImplementedError


# CLASS ENFANT
class BalanceGraph(BaseGraph):

    def __init__(self):
        super().__init__()
        self.title = "Bilan Annuel"
        self.x_label = "Mois"
        self.y_label = "Chiffre d'affaire"

