from Class.Individual.employee import Employee
from Class.Corporation.firm import Firm
from Class.Individual.person import Person
from Class.Equipment.car import Car
from Class.Equipment.office import Office
from Class.Equipment.furnitures import Furnitures
from Display.Graph import BaseGraph, BalanceGraph
from File.File import File
from Display.Graph import BalanceGraph

import json
import random


def load_data_firm():

    data = File()
    data.initialisation()
    return data.list_firm


    # mise à jour des dépenses de chaque entreprise
    #for firm in list_firm:
     #   firm.update_monthly_outlay()
        # firm.show_details()



# SIMULATION SUR 12 ITERATIONS - SYSTEM DE RATING

def Simulation(list_firm, month):

    for firm in list_firm:
        pass
    #graph1 = BalanceGraph(firm.name)
    #graph2 = BalanceGraph()
    #graph3 = BalanceGraph

    for firm in list_firm:
        tab = []
        tab2 = []
        for i in range(month + 1):
            firm.mix_workers()
            firm.update_monthly_outlay()
            firm.update_monthly_income()
            firm.update_capital()

            # firm.show_details()
            tab.append(firm.capital)
            tab2.append(i)

        final = BalanceGraph()
        final.show(tab, tab2)


def refresh_file(list_firm, month):
    list_employee = []
    list_firm = []
    list_person = []
    list_car = []
    list_office = []
    with open("data.json") as my_file:
        data = json.load(my_file)

        # création d'une liste de bureaux d'entreprise
        for firm_office_dict in data['Firm_Office']:
            for value in firm_office_dict.values():
                for about in value:
                    office = Office(about["Name"], about["Price"], about["Owner"], about["Rent"])
                    list_office.append(office)

        # création d'une liste de voitures de fonction
        for firm_car_dict in data['Firm_Car']:
            for value in firm_car_dict.values():
                for about in value:
                    car = Car(about["Model"], about["Plate_Number"],
                              about["Price"], about["Monthly_charge"], about["Owner"])
                    list_car.append(car)

        # création d'une liste de personnes hors entreprise
        for person_dict in data['Person']:
            for value in person_dict.values():
                for about in value:
                    person = Person(about["LastName"], about["FirstName"], about["Age"], about["Adress"],
                                    about["Phone"], about["Mail"])
                    list_person.append(person)

        # création d'une liste d'entreprises
        for firm_dict in data['Firm']:
            for value in firm_dict.values():
                for about in value:
                    firm = Firm(about["Name"])
                    list_firm.append(firm)

        # création d'une liste d'employés
        for employee_dict in data['Employee']:
            for value in employee_dict.values():
                for about in value:
                    employee = Employee(about["LastName"], about["FirstName"], about["Age"], about["Adress"],
                                        about["Phone"], about["Mail"], about["Firm_id"], about["Monthly_wage"],
                                        about["Firm"])
                    list_employee.append(employee)

    # Ajouter les employées, les bureaux et les voitures respectivement à chaque entreprise
    for firm in list_firm:
        for employee in list_employee:
            if employee.firm == firm.name:
                firm.hire(employee)

        for office in list_office:
            if office.owner == firm.name:
                firm.buy_office(office)

        for car in list_car:
            if car.owner == firm.name:
                firm.buy_car(car)

        firm.update_monthly_outlay()

    # mise à jour des dépenses de chaque entreprise
    for firm in list_firm:
        firm.update_monthly_outlay()
        # firm.show_details()

    return list_firm

###########################################################
###| SIMULATION SUR 12 ITERATIONS - SYSTEME DE RATING |####
###########################################################

def simulate(list_firm, month):
    for firm in list_firm:
        tab = []
        tab2 = []
        for i in range(month+1):
            firm.workers()
            firm.update_monthly_outlay()
            firm.update_monthly_income(random.randint(6000, 10000))
            firm.update_capital()

            # firm.show_details()
            tab.append(firm.capital)
            tab2.append(i)

        final = BalanceGraph()
        final.show(tab, tab2)


