from Class.Individual.employee import Employee
from Class.Equipment.office import Office
from Class.Equipment.car import Car
from Class.Equipment.furnitures import Furnitures

import random

class Firm:
    def __init__(self, name: str = "", monthly_outlay: float= 0):
        self._name = name
        self._monthly_outlay = monthly_outlay
        self._monthly_income = 0
        self._employee_list = []
        self._offices_list = []
        self._cars_list = []
        self._capital = 0

    ###Getters & Setters###
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def monthly_outlay(self):
        return self._monthly_outlay

    @monthly_outlay.setter
    def monthly_outlay(self, value):
        self._monthly_outlay = value

    @property
    def monthly_income(self):
        return self._monthly_income

    @monthly_income.setter
    def monthly_income(self, value):
        self._monthly_income = value

    @property
    def employee_list(self):
        return self._employee_list

    @employee_list.setter
    def employee_list(self, value):
        self._employee_list = value

    @property
    def offices_list(self):
        return self._offices_list

    @offices_list.setter
    def offices_list(self, value):
        self._offices_list = value

    @property
    def cars_list(self):
        return self._cars_list

    @cars_list.setter
    def cars_list(self, value):
        self._cars_list = value

    @property
    def capital(self):
        return self._capital

    @capital.setter
    def capital(self, value):
        self._capital = value

    ###Methods###
    #####| Concernant la firme |#####
    def show_details(self):
        print(f"\nDétails Entreprise :"
              f"\nNom de la firme : {self.name}"
              f"\nNombre d'employés : {len(self.employee_list)}"
              f"\nNombre de voitures de fonction : {len(self.cars_list)}"
              f"\nNombre de bureaux : {len(self.offices_list)}"
              f"\nRevenu mensuel : {self.monthly_income} €\n")

    #Calcul des dépenses, revenues et capital de l'entreprise
    def update_monthly_outlay(self):
        value = 0
        for employee in self._employee_list:
            value -= employee._monthly_wage
        for office in self._offices_list:
            value -= office._monthlyCost
        for car in self._cars_list:
            value -= car._monthlyCost
        self._monthly_outlay = value

    def update_monthly_income(self):
        self.monthly_income = random.randint(5000,20000)

    def update_capital(self):
        self.capital = self.capital + self.monthly_income + self.monthly_outlay

    #####| Concernant les employés |#####
    #embaucher un employé
    def hire(self, employee:Employee):
        self.employee_list.append(employee)

    def mix_workers(self):
        for employee in self.employee_list:
            random_Num = random.randint(0, 3)
            if random_Num >= 1:
                employee.job_is_done()
            if random_Num == 0:
                employee.job_is_not_done()
            if employee.check_rating() == False:
                self.fire(employee)

    #débaucher un employé
    def fire(self, fired_employee: Employee):
        for employee in self.employee_list:
            if fired_employee == employee:
                self.employee_list.remove(employee)

    #####| Concernant les bureaux |#####
    #acheter un bureau
    def buy_office(self, office: Office):
        self.offices_list.append(office)
        self.capital -= office.value

    #vendre un bureau
    def sell_office(self, sold_office: Office):
        for office in self.offices_list:
            if office == sold_office:
                self.offices_list.remove(office)
                self.capital += office.value/0.90

    #####| Concernant les voitures |#####
    #acheter une voiture
    def buy_car(self, car: Car):
        self._cars_list.append(car)
        self._capital -= car._value

    #vendre une voiture
    def sell_car(self, sold_car: Car):
        for car in self.cars_list:
            if sold_car == car:
                self.cars_list.remove(car)
                self.capital += car.value/0.90
