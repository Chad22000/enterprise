from Class.Individual.person import Person

class Employee(Person):

    def __init__(self, last_name:str, first_name: str, age: int, adress: str, phone: str, mail: str, firm_id: int, monthly_wage: float, firm: str):
        Person.__init__(self, last_name, first_name, age, adress, phone, mail)
        self._firm_id = firm_id
        self._monthly_wage = monthly_wage
        self._boss_rating = 75
        self._firm = firm

    ###Getters & Setters###
    @property
    def firm_id(self):
        return self._firm_id

    @firm_id.setter
    def firm_id(self, value):
        self._firm_id = value

    @property
    def monthly_wage(self):
        return self._monthly_wage

    @monthly_wage.setter
    def monthly_wage(self, value):
        self._monthly_wage = value

    @property
    def boss_rating(self):
        return self._boss_rating

    @boss_rating.setter
    def boss_rating(self, value):
        self._boss_rating = value

    @property
    def firm(self):
        return self._firm

    @firm.setter
    def firm(self, value):
        self._firm = value

    ###Methods###
    def raise_salary(self, raise_salary):
        self.monthly_wage = self.monthly_wage + raise_salary

    def enter_firm(self, firm, firm_id):
        self.firm = firm
        self.firm_id = firm_id

    def quit(self):
        self.firm = None

    def job_is_done(self):
        self.boss_rating += 5

    def job_is_not_done(self):
        self.boss_rating -= 10

    def check_rating(self):
        if self.boss_rating > 110:
            answer = True
            #print("bon travail", self.first_name)
        elif self.boss_rating < 30:
            answer = False
            print("Tu es viré", self.first_name)
            self.Quit()
        else:
            answer= True
            #print("RAS", self.first_name)
        return answer

    def show_employee_details(self):
        print(f"\nDétails Salarié :"
              f"\nid : {self.firm_id}"
              f"\nSalaire mensuel : {self.monthly_wage} €"
              f"\nSatisfaction du PDG : {self.boss_rating}\n")




