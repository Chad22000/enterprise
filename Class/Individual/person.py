class Person:

    def __init__(self, last_name:str, first_name:str, age:int, adress:str, phone:str, mail:str):
        self._last_name = last_name
        self._first_name = first_name
        self._age = age
        self._adress = adress
        self._phone = phone
        self._mail = mail

    ###Getters & Setters###
    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value
        
    @property
    def last_name(self):
        return self._last_name

    @last_name.setter
    def last_name(self, value):
        self._last_name = value

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, value):
        self._first_name = value

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, value):
        self._age = value

    @property
    def adress(self):
        return self._adress

    @adress.setter
    def adress(self, value):
        self._adress = value

    @property
    def phone(self):
        return self._phone

    @phone.setter
    def phone(self, value):
        self._phone = value

    @property
    def mail(self):
        return self._mail

    @mail.setter
    def mail(self, value):
        self._mail = value

    ###Methods###
    def get_id(self):
        return self.id

    def get_last_name(self):
        return self.last_name

    def get_first_name(self):
        return self.first_name

    def get_age(self):
        return self.age

    def get_adress(self):
        return self.adress

    def get_phone(self):
        return self.phone

    def to_apply(self, firm):
        self.apply_to_firm = True
        return firm
