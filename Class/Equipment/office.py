from Class.Equipment.furnitures import Furnitures

class Office(Furnitures):
    def __init__(self, value: float, monthCost: float, name: str, owner: str):
        Furnitures.__init__(self, value, monthCost)
        self._name = name
        self._owner = owner
        self._occupied = bool

    ###Getters & Setters###
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, value):
        self._owner = value

    @property
    def occupied(self):
        return self._occupied

    @occupied.setter
    def occupied(self, value):
        self._occupied = value

    ###Methods###
    def is_occupied(self, occupied: bool):
        if occupied:
            self.price = None
        else:
            self.price = int


