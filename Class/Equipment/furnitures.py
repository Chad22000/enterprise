class Furnitures():
    def __init__(self, value: float, monthCost: float):
        self._value = value
        self._monthlyCost = monthCost

    ###Getters & Setters###

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, new_value):
        self._value = new_value

    @property
    def monthlyCost(self):
        return self._monthlyCost

    @monthlyCost.setter
    def monthlyCost(self, value):
        self._monthlyCost = value

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    ###Methods###
    def get_id(self):
        return self.id
