from Class.Equipment.furnitures import Furnitures

class Car(Furnitures):
    def __init__(self,value: float, monthlyCost: float, model: str, plate_number: str, owner: str):
        Furnitures.__init__(self, value, monthlyCost)
        self._model = model
        self._plate_number = plate_number
        self._owner = owner
        self._firm_car = None

    ###Getters & Setters###
    @property
    def model(self):
        return self._model

    @model.setter
    def model(self, value):
        self._model = value

    @property
    def plate_number(self):
        return self._plate_number

    @plate_number.setter
    def plate_number(self, value):
        self._plate_number = value

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, value):
        self._owner = value


    @property
    def firm_car(self):
        return self._firm_car

    @firm_car.setter
    def firm_car(self, value):
        self._firm_car = value

    ###Methods###
    def has_owner(self):
        if self.owner is None:
            print("La voiture " + self.plate_number + " n'a pas de propriétaire")
            return False
        else:
            return True

    def firm_car(self, firm):
        self._firm_car = firm.name
        firm.cars_list.append(self)
