import json
from Class.Equipment.car import Car
from Class.Individual.employee import Employee
from Class.Individual.person import Person
from Class.Equipment.office import Office
from Class.Corporation.firm import Firm


class File:

    def __init__(self):
        self.list_employee = []
        self.list_firm = []
        self.list_car = []
        self.list_office = []

    def initialisation(self):
        self.get_firm()
        self.get_employee()
        self.get_car()
        self.get_office()
        self.load_object()

    def get_office(self):
        with open("data.json") as my_file:
            data = json.load(my_file)
            # création d'une liste de bureaux d'entreprise
            for firm_office_dict in data['Office']:
                for value in firm_office_dict.values():
                    for about in value:
                        office = Office(about["Price"], about["Rent"], about["Name"], about["Owner"])
                        self.list_office.append(office)

    def get_car(self):
        with open("data.json") as my_file:
            data = json.load(my_file)
            # création d'une liste de voitures de fonction
            for firm_car_dict in data['Car']:
                for value in firm_car_dict.values():
                    for about in value:
                        car = Car(about["Price"], about["Monthly_charge"], about["Model"], about["Plate_Number"], about["Owner"])
                        self.list_car.append(car)

    def get_person(self):
        with open("data.json") as my_file:
            data = json.load(my_file)
            # création d'une liste de personnes hors entreprise
            for person_dict in data['Person']:
                for value in person_dict.values():
                    for about in value:
                        person = Person(about["LastName"], about["FirstName"], about["Age"], about["Adress"],
                                        about["Phone"], about["Mail"])
                        self.list_person.append(person)

    def get_firm(self):
        with open("data.json") as my_file:
            data = json.load(my_file)
            # création d'une liste d'entreprises
            for firm_dict in data['Firm']:
                for value in firm_dict.values():
                    for about in value:
                        firm = Firm(about['Name'])
                        self.list_firm.append(firm)

    def get_employee(self):
        with open("data.json") as my_file:
            data = json.load(my_file)
            # création d'une liste d'employés
            for employee_dict in data['Employee']:
                for value in employee_dict.values():
                    for about in value:
                        employee = Employee(about["LastName"], about["FirstName"], about["Age"], about["Adress"],
                                            about["Phone"], about["Mail"], about["Firm_id"], about["Monthly_wage"],
                                            about["Firm"])
                        self.list_employee.append(employee)

    def write_file(self):
        with open("data2.json", 'w', encoding='utf-8') as my_file:
            data = json.load(my_file)
            data.dump(self)

    def load_object(self):
        # Ajouter les employées, les bureaux et les voitures respectivement à chaque entreprise
        for firm in self.list_firm:
            for employee in self.list_employee:
                if employee.firm == firm.name:
                    firm.hire(employee)

            for office in self.list_office:
                if office.owner == firm.name:
                    firm.buy_office(office)

            for car in self.list_car:
                if car.owner == firm.name:
                    firm.buy_car(car)

